#!/bin/bash -
#===============================================================================
#
#          FILE: Condor Convenience Commands
#
#
#   DESCRIPTION:
#
# 		 - NOTE : source me AT YOUR OWN RISK like this : `. cococo.sh` 
#
#                - condor            : Prefix functions with `sudo -u <PROC-USER>`
#                - htc.submitters    : condor condor_status -submitters
#                - htc.status        : condor condor_status
#                - htc.statusrun     : condor condor_status -run
#                - htc.submit        : condor condor_submit
#                - htc.query         : condor condor_q
#                - htc.tail          : condor condor_tail
#                - htc.tailfollow    : condor condor_tail -f
#                - htc.analyze       : condor condor_q -analyze
#                - htc.analyzebetter : condor condor_q -better-analyze
#                - htc.ssh_to_job    : condor condor_ssh_to_job -auto-retry
#                - htc.hold          : condor condor_hold
#                - htc.release       : condor condor_release
#                - htc.remove        : condor condor_rm
#                - uncococo      : Unset all abovementioned functions
#
#  REQUIREMENTS: bash, HTCondor 
#
#         To Do: Test! It works for me.-
#
#        AUTHOR: Nikos Alexandris
#  ORGANIZATION: Project Officer, PVGIS, C2, JRC, EC
#
#       CREATED: Sun 20 Nov 2022
#      REVISION: See git commit logs
#===============================================================================

# set -uo pipefail
# set -o nounset                             # Treat unset variables as an error


# Sourcing me without the only required argument, I will show the following :::::

# check whether user had supplied -h or --help . If yes display usage
if [[ ( $* == "--help") ||  $* == "-help" ||  $* == "-h" ]] || [[ -z ${1+set} ]] ;then
    echo "COndor.COnvenience.COmmands : use AT YOUR OWN RISK"
    PROC_USER='pvgisproc'
    echo "Using default username      : ${PROC_USER}" 
    echo "Another username? Use       : . cococo.sh <oTHERuSERNAME>"
    echo "To unset, use the function  : uncococo"
else
    PROC_USER="$1"
    echo "Username given: ${PROC_USER}" 
fi


function condor() {
    sudo -u "$PROC_USER" "${@}"
}


function htc.submitters() {
    condor condor_status -submitters
}
export -f htc.submitters


function htc.status() {
    condor condor_status "${@}"
}
export -f htc.status


function htc.statusrun() {
    condor condor_status -run
}
export -f htc.statusrun


function htc.submit() {
    condor condor_submit "${@}"
}
export -f htc.submit


function htc.query() {
    condor condor_q -nobatch "${@}" 
}
export -f htc.query


function htc.tail() {
    condor condor_tail "${@}" 
}
export -f htc.tail


function htc.tailfollow() {
    condor condor_tail -f "${@}" 
}
export -f htc.tailfollow


function htc.analyze() {
    condor condor_q -analyze "${@}" 
}
export -f htc.analyze


function htc.analyzebetter() {
    condor condor_q -better-analyze "${@}" 
}
export -f htc.analyzebetter


function htc.ssh_to_job() {
    condor condor_ssh_to_job -auto-retry "${@}" 
}
export -f htc.ssh_to_job


function htc.hold() {
    condor condor_hold pvgisproc "${@}" 
}
export -f htc.hold


function htc.release() {
    condor condor_release pvgisproc "${@}" 
}
export -f htc.release


function htc.remove() {
    # example parameters : -constraint 'ClusterId > 49000'
    condor condor_rm "${@}"
}
export -f htc.remove


function uncococo() {
    # Unset functions
    unset -f htc.submitters
    unset -f htc.status
    unset -f htc.statusrun
    unset -f htc.submit
    unset -f htc.query
    unset -f htc.tail
    unset -f htc.tailfollow
    unset -f htc.analyze
    unset -f htc.analyzebetter
    unset -f htc.ssh_to_job
    unset -f htc.hold
    unset -f htc.release
    unset -f htc.remove
    unset -f uncococo  # self!
}
export -f uncococo
