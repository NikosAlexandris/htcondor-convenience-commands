# Working with HTCondor

Certainty may be self-deception. Before you start, verify...

1. Already inside an HTCondor environment?
2. Job description files ready?
3. If yes and happy, submit jobs...

## CoCoCo

Set the following `pvgisproc`-specific HTCondor convenience commands

- `condor`            : Prefix functions with `sudo -u <PROC-USER>`
- `htc.submitters`    : `condor condor_status -submitters`
- `htc.status`        : `condor condor_status`
- `htc.statusrun`     : `condor condor_status -run`
- `htc.submit`        : `condor condor_submit`
- `htc.query`         : `condor condor_q`
- `htc.tail`          : `condor condor_tail`
- `htc.tailfollow`    : `condor condor_tail -f`
- `htc.analyze`       : `condor condor_q -analyze`
- `htc.analyzebetter` : `condor condor_q -better-analyze`
- `htc.ssh_to_job`    : `condor condor_ssh_to_job -auto-retry`
- `htc.hold`          : `condor condor_hold`
- `htc.release`       : `condor condor_release`
- `htc.remove`        : `condor condor_rm`
- `uncococo`          : Unset all abovementioned functions

via
```
. cococo.sh
```

and unset when/if need be via

```
. uncococo.sh
```

Try, then, to run for example :

- `htc.submitters` <- who and how many jobs?

## Files

.
├── cococo.sh   : Set Condor Convenieve Commands at your fingertips
└── uncococo.sh : Unset CoCoCo 
